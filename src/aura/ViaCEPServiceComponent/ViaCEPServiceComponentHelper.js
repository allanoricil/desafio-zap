({
	getAddressUsingCep : function(cmp, cep) {
		var action = cmp.get('c.getAddressUsingCep');
        action.setParams({
            cep : cep
        });
        action.setCallback(this, function(response){
           var state = response.getState();
            if(state === "SUCCESS"){
                cmp.set('v.address', response.getReturnValue());
                if(cmp.get('v.address') == null){
                   this.showToast('error', 'Endereço não foi encontrado para o CEP digitado');
                }
            }else if(state === "ERROR"){
                this.showToast('error', 'Endereço não foi encontrado para o CEP digitado');     
            }
        });
        $A.enqueueAction(action);
	},
    saveAddressOnAccount : function(cmp){
        var action = cmp.get('c.saveAddressOnAccount');
        action.setParams({
            accountId : cmp.get('v.recordId'),
            addressJson : JSON.stringify(cmp.get('v.address')),
            isBillingAddress : cmp.get('v.isBillingAddress')
        });
        action.setCallback(this, function(response){
           var state = response.getState();
            if(state === "SUCCESS"){
                cmp.set('v.address', response.getReturnValue());
                var closeQuickActionEvent = $A.get("e.force:closeQuickAction");
                if(closeQuickActionEvent != null){
        			closeQuickActionEvent.fire();
                }
                this.showToast('success', 'Endereço atualizado com sucesso!');
                $A.get("e.force:refreshView").fire();
            }else if(state === "ERROR"){
                this.showToast('error', 'Endereço não foi atualizado');
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(type, message){
        var toastEvent = $A.get("e.force:showToast");
        var title;
        if(type === 'error'){
            title = 'Erro';
        }else{
            title = 'Sucesso';
        }
        if(toastEvent != null){
            toastEvent.setParams({
                title : title,
                message: message,
                type: type,
                mode: 'sticky'
            });
            toastEvent.fire();
        }
    }
})