({
	searchAddress : function(cmp, event, helper){
        var cepInput = cmp.find('cep-input');
        var cep = cmp.get('v.cep');
        if(/^\d{8}$/.test(cep)){
        	helper.getAddressUsingCep(cmp, cep);
            cepInput.setCustomValidity('');
        }else{
            cepInput.setCustomValidity('O cep deve conter apenas números.');
        }
        cepInput.reportValidity();
    },
    onSave : function(cmp, event, helper){
        helper.saveAddressOnAccount(cmp);
    },
    onCancel : function(cmp, event, helper){
    	$A.get("e.force:closeQuickAction").fire();
    },
    handleRadioClick : function(cmp, event, helper){
        var radioButtonValue = event.getSource().get('v.value');
        if(radioButtonValue === '1'){
            cmp.set('v.isBillingAddress', true);
        }else{
            cmp.set('v.isBillingAddress', false);
        }
    }
})