public class ViaCEPService {
	
    private static final String VIA_CEP_WS_ENDPOINT = 'https://viacep.com.br/ws/';

    //get
    public ViaCEPServiceResponseWrapper getAddressGivenCEP(String cep){
        //backend validation
        if(ViaCEPServiceHelper.isCepValid(cep)){
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(VIA_CEP_WS_ENDPOINT + cep +'/json/');
            request.setMethod('GET');
            try{
                HttpResponse response = http.send(request);
                if(response.getStatusCode() == 200){
                    Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                    if(results != null && !results.containsKey('erro')){
                        return ViaCEPServiceResponseWrapper.parse(response.getBody());  
                    }
                }
            }catch(CalloutException ex){
            	return null;
            }
        }
        
        return null;
    }
}