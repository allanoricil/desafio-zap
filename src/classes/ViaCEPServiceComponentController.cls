public class ViaCEPServiceComponentController {
	
    @AuraEnabled
    public static ViaCEPServiceResponseWrapper getAddressUsingCep(String cep){
        ViaCEPService cepService = new ViaCEPService();
        ViaCEPServiceResponseWrapper address = cepService.getAddressGivenCEP(cep);
        return address;
    }
    
    @AuraEnabled
    public static Boolean saveAddressOnAccount(String accountId, String addressJson, Boolean isBillingAddress){
        ViaCEPServiceResponseWrapper address = (ViaCEPServiceResponseWrapper)JSON.deserialize(addressJson, ViaCEPServiceResponseWrapper.class);
        Account account = new Account(Id = accountId);
        if(isBillingAddress){
            account.BillingPostalCode = address.getCep();
            account.BillingCity = address.getLocalidade();
            account.BillingStreet = address.getLogradouro();
            account.BillingState = address.getUf();
        }else{
            account.ShippingPostalCode = address.getCep();
            account.ShippingCity = address.getLocalidade();
            account.ShippingStreet = address.getLogradouro();
            account.ShippingState = address.getUf();
        }
        
        try{
            update account;
            return true;
        }catch(DmlException ex){
            return false;
        }
    }
    
}