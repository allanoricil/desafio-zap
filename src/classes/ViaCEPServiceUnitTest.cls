@isTest
public class ViaCEPServiceUnitTest {

    @isTest
    static void TestaChamadaDoWebServiceGetAddressComCepValido(){
   		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ViaCEPServiceMock());
       	
        ViaCEPService cepService = new ViaCEPService();
        ViaCEPServiceResponseWrapper address = cepService.getAddressGivenCEP('12220750');
        
        System.assertEquals('12220-750',address.getCep());
        System.assertEquals('Praça da Sé', address.getLogradouro());
        System.assertEquals('lado ímpar',address.getComplemento());
        System.assertEquals('Sé',address.getBairro());
        System.assertEquals('São Paulo',address.getLocalidade());
        System.assertEquals('SP',address.getUf());
        System.assertEquals('',address.getUnidade());
        System.assertEquals('3550308',address.getIbge());
        System.assertEquals('1004',address.getGia());
        
        Test.stopTest();
    }
    
    @isTest
    static void TestaChamadaDoWebServiceGetAddressComCepValidoInexistente(){
    	Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ViaCEPServiceMock());
	
		ViaCEPService cepService = new ViaCEPService();        
        ViaCEPServiceResponseWrapper address = cepService.getAddressGivenCEP('99999999');
        System.assertEquals(null, address);
        
        Test.stopTest();
    }
    
    @isTest
    static void TestaChamadaDoWebServiceQuandoOcorreUmaExcessaoDeChamada(){
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new ViaCEPServiceMock());
        
        ViaCEPService cepService = new ViaCEPService();
		ViaCEPServiceResponseWrapper address = null;      
        try{
        	address = cepService.getAddressGivenCEP('12220751'); 
        }catch(CalloutException ex){
            System.assertEquals(null, address);
        }
        
		Test.stopTest();        
    }
    
    @isTest
    static void TestaChamadaDoWebServiceGetAddressComCepInvalido(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ViaCEPServiceMock());
	
		ViaCEPService cepService = new ViaCEPService();        
        ViaCEPServiceResponseWrapper address = cepService.getAddressGivenCEP('999999990');
        System.assertEquals(null, address);
        
        address = cepService.getAddressGivenCEP('9999A9999');
        System.assertEquals(null, address);
        
        address = cepService.getAddressGivenCEP('999A 913');
        System.assertEquals(null, address);
        
        Test.stopTest();
    }
    
    
    @isTest
    static void TestaAValidacaoDoFormatoDoCepQuandoValido(){
        Test.startTest();
        
        String cep = '12220750';
        System.assertEquals(true, ViaCEPServiceHelper.isCepValid(cep));
        
        Test.stopTest();
    }
    
    @isTest
    static void TestaAValidacaoDoFormatoDoCepQuandoInvalido(){
        Test.startTest();
        
        String cep = '999999990';
        System.assertEquals(false, ViaCEPServiceHelper.isCepValid(cep));
        
        cep = '9999A9999';
        System.assertEquals(false, ViaCEPServiceHelper.isCepValid(cep));
        
        cep = '999A 913';
        System.assertEquals(false, ViaCEPServiceHelper.isCepValid(cep));
        
        Test.stopTest();
    }
    
}