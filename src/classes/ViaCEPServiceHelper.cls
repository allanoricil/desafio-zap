public abstract class ViaCEPServiceHelper {

    public static Boolean isCepValid(String cep){
        Pattern cepPattern = Pattern.compile('[0-9]{8}?');
        Matcher cepPatternMatcher = cepPattern.matcher(cep);
        return cepPatternMatcher.matches();
    }
    
}