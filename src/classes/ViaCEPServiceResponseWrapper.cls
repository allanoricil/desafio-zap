public class ViaCEPServiceResponseWrapper {
	
    @AuraEnabled
    private String cep{get;set;}
    @AuraEnabled
    private String logradouro{get;set;}
    @AuraEnabled
    private String complemento{get;set;}
    @AuraEnabled
    private String bairro{get;set;}
    @AuraEnabled
    private String localidade{get;set;}
    @AuraEnabled
    private String uf{get;set;}
    @AuraEnabled
    private String unidade{get;set;}
    @AuraEnabled
    private String ibge{get;set;}
    @AuraEnabled
    private String gia{get;set;}
    
    public ViaCEPServiceResponseWrapper(String cep,String logradouro,String complemento, String bairro, String localidade, String uf, String unidade, String ibge, String gia){
        this.cep = cep;
        this.logradouro = logradouro;
        this.complemento = complemento;
        this.bairro = bairro;
        this.localidade = localidade;
        this.uf = uf;
        this.ibge = ibge;
        this.gia = gia;
    }
    
    public static ViaCEPServiceResponseWrapper parse(String payload){
        return (ViaCEPServiceResponseWrapper)System.JSON.deserialize(payload, ViaCEPServiceResponseWrapper.class); 
    }
    
    public String getCep(){
        return cep;
    }
    
    public String getLogradouro(){
        return logradouro;
    }
    public String getComplemento(){
        return complemento;
    }
    public String getBairro(){
        return bairro;
    }
    public String getLocalidade(){
        return localidade;
    }
    public String getUf(){
        return uf;
    }
    public String getUnidade(){
        return unidade;
    }
    public String getIbge(){
        return ibge;
    }
    public String getGia(){
        return gia;
    }
    
}