@isTest
public class ViaCEPServiceIntegrationTest {

    @isTest
    static void TestaAAtualizacaoDoEnderecoDeEntregaDaConta(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ViaCEPServiceMock());
        ViaCEPServiceResponseWrapper address = ViaCEPServiceComponentController.getAddressUsingCep('12220750');
        Account a = criaContaTeste();
        Boolean atualizouConta = ViaCEPServiceComponentController.saveAddressOnAccount(a.Id, JSON.serialize(address), true);
        System.assertEquals(true, atualizouConta);
        
        a = [SELECT BillingPostalCode, 
             		BillingCity, 
             		BillingStreet, 
             		BillingState 
               FROM Account 
              WHERE Id =:a.Id];
        
        System.assertEquals('12220-750', a.BillingPostalCode);
        System.assertEquals('São Paulo', a.BillingCity);
        System.assertEquals('Praça da Sé', a.BillingStreet);
        System.assertEquals('SP', a.BillingState);
            
        Test.stopTest();
    }
    
    @isTest
    static void TestaAAtualizacaoDoEnderecoDeCobrancaDaConta(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ViaCEPServiceMock());
        ViaCEPServiceResponseWrapper address = ViaCEPServiceComponentController.getAddressUsingCep('12220750');
        Account a = criaContaTeste();
        Boolean atualizouConta = ViaCEPServiceComponentController.saveAddressOnAccount(a.Id, JSON.serialize(address), false);
        System.assertEquals(true, atualizouConta);
        
        a = [SELECT ShippingPostalCode, 
             		ShippingCity, 
             		ShippingStreet,
             		ShippingState 
               FROM Account 
              WHERE Id =:a.Id];
	    
        System.assertEquals('12220-750', a.ShippingPostalCode);
        System.assertEquals('São Paulo', a.ShippingCity);
        System.assertEquals('Praça da Sé', a.ShippingStreet);
        System.assertEquals('SP', a.ShippingState);
        
        Test.stopTest();
    }
    
    private static Account criaContaTeste(){
        Account a = new Account();
        a.Name = 'Test';
        insert a;
        return a;
    }
    
}