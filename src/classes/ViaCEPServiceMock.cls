global class ViaCEPServiceMock implements HttpCalloutMock{
	
    
    global HttpResponse respond(HttpRequest request){	        
        HttpResponse response = new HttpResponse();

        String body = null;
        Integer statusCode = 400;
        
        if(request.getMethod() == 'GET'){
            List<String> endpointTokens = request.getEndpoint().split('/');
            if(endpointTokens != null && endpointTokens.contains('viacep.com.br') && endpointTokens.size() == 6){
                String cep = endpointTokens.get(4);
                statusCode = 200;
                if(cep.equals('99999999')){
                    body = '{"erro" : true}';
                }else if(cep.equals('12220750')){
                    body = '{"cep":"12220-750","logradouro":"Praça da Sé","complemento":"lado ímpar","bairro":"Sé","localidade":"São Paulo","uf":"SP","unidade":"","ibge":"3550308","gia":"1004"}';
                }else{
                    throw new CalloutException();
                }
            }
        }

        response.setBody(body);
        response.setStatusCode(statusCode);
    	return response;
    }
    
    
    
}