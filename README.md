
#Desafio Grupo Zap#

Desenvolvido por Allan Gonçalves Gomes Oricil, Engenheiro de Computação formado pela Universidade Federal de Itajubá. Trabalha atualmente como Desenvolvedor Salesforce.


##Problema 1:##


Para a primeira parte do desafio eu desenvolvi a integração com o ViaCEP.Criei um serviço e deixei exposto para ser utilizado através do objeto ViaCEPService. Esse serviço valida o cep utilizando Regex. Isso foi feito para respeitar o padrão de segurança por dupla validação. A primeira no backend e a segunda no frontend.


Exemplo de uso:


```java
ViaCEPServiceResponseWrapper address = new ViaCEPService().getAddressGivenCEP('12220750');
```
O retorno é um objeto address que utiliza a class ViaCEPServiceResponseWrapper para desserializar os dados em atributos de objeto no backend. Com o objeto address em mãos é possível pegar todos os atributos de endereço utilizando os seguintes métodos:


```java
- public String getCep();

- public String getLogradouro();

- public String getComplemento();

- public String getBairro();

- public String getLocalidade();

- public String getUf();

- public String getUnidade();

- public String getIbge();

- public String getGia();
```


##Problema 2:##

Para a segunda parte do desafio eu desenvolvi um lightning component para ser utilizado em uma QuickAction.  Primeiro eu criei toda a estrutura do lightning component para receber os inputs necessários. O Componente pode ser visto abaixo:

![picture](src/component.JPG) 

O campo que recebe CEP é do tipo "text". Ele aceita no mínimo e no máximo 8 caracteres. Caso o usuário digite menos de 8 digitos e aperte o botão "procurar" ou salvar, será exibida a seguinte mensagem de erro "O Cep precisa ter 8 caracteres". Caso o usuário digite um cep não válido, será exibida a seguinte mensagem de erro "O cep deve conter apenas números.". Essa validação foi feita via Regex no frontend. Caso o CEP seja válido e exista no serviço do ViaCEP será retornado um objeto "address" não nulo. Caso o CEP seja válido e não exista no serviço do ViaCEP será retornado um objeto "address" nulo. Toda vez que o endereço é encontrado ele é exibido em detalhes no meio do componente.

Para salvar esse endereço em algum dos campos de endereço de um registro "Account", o usuário precisa selecionar algum dos Radio Buttons que indicam em qual seção de endereço ele deseja salvar. Existem duas opções válidas, o BillingAddress e o ShippingAddress. Além disso, antes de clicar no botão Salvar é preciso ter um endereço válido sendo exibido. Ao clicar em  salvar o evento de "refreshView" é disparado e os dados do endereço poderão ser visualizados no registro. O botão cancelar apenas fecha o Modal da QuickAction sem realizar nenhuma Operação.


##Testes:##

Foram desenvolvidos diversos testes unitários e de integração para a solução. Eu tenho o costume de deixar os nomes dos testes
explicando exatamente o que eles fazem para facilitar a leitura e manutenção. Os Testes unitários verificam a validade dos métodos do serviço, do helper e do wrapper. Os testes de integração validam os métodos do controlador do componente, que utilizam os métodos testados nos testes unitários. Eu sempre gosto de separar em duas classes esses testes para separar as responsábilidades de cada um. Como eu desenvolvo em Ruby on Rails eu acabo utilizando uma estrtura de testes similar ao do framework Rails.


##Deploy:##

Para fazer deploy em sua organização basta utilizar o "package.xml" e o conteúdo da pasta "src". Com isso você terá disponível
a QuickAction para ser utilizada com o objeto Account. Eu gosto de utilizar o Ant Migration Tool junto com o Pipelines do Bitbucket para fazer deploys, e até mesmo aplicar CI/CD.



